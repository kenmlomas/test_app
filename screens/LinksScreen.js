import React from "react";
import { ScrollView, StyleSheet, View, Text, Button } from "react-native";
import * as Progress from "react-native-progress";
import { getTotal } from "./functions";

export default class LinksScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total: 0
    };
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  componentDidMount() {
    this.setState({ total: getTotal() });
  }

  static navigationOptions = {
    title: "Links"
  };

  render() {
    // const { } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container}>
          <View style={styles.getStartedContainer}>
            <Text style={styles.getStartedText}>
              Progress Bar: {this.state.total}
            </Text>
            <Progress.Bar progress={this.state.total / 100} width={200} />
            <Button
              onPress={() => this.props.navigation.navigate("HomeStack")}
              title="Back home!"
              color="#841584"
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff"
  }
});
