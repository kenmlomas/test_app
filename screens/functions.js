var total = 0;

export function plusTotal(x) {
  total += x;
}

export function getTotal() {
  return total;
}
